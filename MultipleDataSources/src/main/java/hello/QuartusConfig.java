//package hello;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.orm.jpa.JpaTransactionManager;
//import org.springframework.orm.jpa.JpaVendorAdapter;
//import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
//import org.springframework.transaction.PlatformTransactionManager;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//
//import javax.persistence.EntityManager;
//import javax.sql.DataSource;
//
//@Configuration
//@EnableTransactionManagement
//@EnableJpaRepositories(
//        entityManagerFactoryRef = "entityManagerFactoryQuartus",
//        transactionManagerRef = "transactionManagerQuartus",
//        basePackages = {"hello.domain.q.repository"})
//@Slf4j
//public class QuartusConfig {
//
//    @Value("${spring.datasource.quartus.type}")
//    private String datasourceType;
//
//    @Bean
//    @ConfigurationProperties(prefix = "spring.datasource.quartus")
//    public DataSource quartusDataSource() {
//        DataSourceBuilder builder = DataSourceBuilder.create();
//        if (null != datasourceType) {
//            Class type = null;
//            try {
//                type = Class.forName(datasourceType);
//            } catch (ClassNotFoundException e) {
//                log.error("DataSource type配置错误，已重置为默认的DataSource。 class not find type=" + datasourceType);
//            }
//            if (type != null) {
//                builder.type(type);
//            }
//        }
//        DataSource dataSource = builder.build();
//        return dataSource;
//    }
//
//    @Autowired
//    JpaVendorAdapter jpaVendorAdapter;
//
//    @Bean
//    public LocalContainerEntityManagerFactoryBean entityManagerFactoryQuartus(EntityManagerFactoryBuilder builder) {
//        LocalContainerEntityManagerFactoryBean emfb = builder
//                .dataSource(quartusDataSource())
//                .packages("hello.domain.q.entity")
//                .persistenceUnit("quartusPersistenceUnit")
//                .build();
//        emfb.setJpaVendorAdapter(jpaVendorAdapter);
//        return emfb;
//    }
//
//    @Bean
//    public EntityManager entityManagerQuartus(EntityManagerFactoryBuilder builder) {
//        return entityManagerFactoryQuartus(builder).getObject().createEntityManager();
//    }
//
//    @Bean
//    PlatformTransactionManager transactionManagerQuartus(EntityManagerFactoryBuilder builder) {
//        return new JpaTransactionManager(entityManagerFactoryQuartus(builder).getObject());
//    }
//}
