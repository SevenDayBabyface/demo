package hello;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "entityManagerFactoryQuartus",
        transactionManagerRef = "transactionManagerQuartus",
        basePackages = {"hello.domain.q.repository"})
@Slf4j
public class QuartusConfig2 {

    @Value("${spring.datasource.default.type}")
    private String datasourceType;

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.default")
    public DataSource tertiaryDataSource() {
        DataSourceBuilder builder = DataSourceBuilder.create();
        if (null != datasourceType) {
            Class type = null;
            try {
                type = Class.forName(datasourceType);
            } catch (ClassNotFoundException e) {
                log.error("DataSource type配置错误，已重置为默认的DataSource。 class not find type=" + datasourceType);
            }
            builder.type(type);
        }
        DataSource dataSource = builder.build();
        return dataSource;
    }

    @Bean
    public JpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
        adapter.setDatabase(Database.MYSQL);
        adapter.setShowSql(false);
        adapter.setGenerateDdl(true);
        adapter.setDatabasePlatform("org.hibernate.dialect.MySQL5Dialect");
        return adapter;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryQuartus(EntityManagerFactoryBuilder builder) {
        DataSource dataSource = tertiaryDataSource();
        LocalContainerEntityManagerFactoryBean lcemfb = builder
                .dataSource(dataSource)
//                .properties(getVendorProperties(dataSource))
                .packages("hello.domain.q.entity")
                .build();
        lcemfb.setJpaVendorAdapter(jpaVendorAdapter());
        return lcemfb;
    }

    @Bean
    public EntityManager entityManagerQuartus(EntityManagerFactoryBuilder builder) {
        EntityManagerFactory entityManagerFactory = entityManagerFactoryQuartus(builder).getObject();
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        return entityManager;
    }

    @Bean
    public PlatformTransactionManager transactionManagerQuartus(EntityManagerFactoryBuilder builder) {
        return new JpaTransactionManager(entityManagerFactoryQuartus(builder).getObject());
    }
}
