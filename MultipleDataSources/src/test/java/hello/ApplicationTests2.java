package hello;

import hello.domain.p.User;
import hello.domain.p.UserRepository;
import hello.domain.t.entity.Role;
import hello.domain.t.repository.RoleRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class ApplicationTests2 {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Test
    public void test() throws Exception {
        userRepository.save(new User("aaa", 10));
        userRepository.save(new User("bbb", 20));

        Assert.assertEquals(2, userRepository.findAll().size());

        roleRepository.save(new Role("平静"));
        Assert.assertEquals(1, roleRepository.findAll().size());
    }
}
