package hello;


import com.alibaba.fastjson.JSON;
import hello.domain.q.entity.Role;
import hello.domain.q.repository.RoleRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class ApplicationTests3 {

    @Autowired
    private RoleRepository roleRepositoryaa;

    @Autowired
    private hello.domain.t.repository.RoleRepository roleRepositoryT;

    @Test
    public void test() throws Exception {
        roleRepositoryaa.save(new Role("平静"));
//        Assert.assertEquals(1, roleRepository.findAll().size());
        List list = roleRepositoryaa.findAll();
        System.out.println(JSON.toJSONString(list));

        roleRepositoryT.save(new hello.domain.t.entity.Role("平静"));
        List listT = roleRepositoryT.findAll();
        System.out.println(JSON.toJSONString(listT));
    }
}
