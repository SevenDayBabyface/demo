package com.example.demo.job.jobs;

import com.example.demo.job.JobBase;
import com.example.demo.job.JobConfig;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
@Slf4j
@DisallowConcurrentExecution //设置不能同时执行同一个job
public class JobRepairOrder extends JobBase {
    public JobConfig getJobConfig() {
        JobConfig jobConfig = new JobConfig();
        jobConfig.setJobName("JobRepairOrder");
        jobConfig.setJobGroupName("JobRepairOrder-group");
        jobConfig.setTriggerName("triggerJobRepairOrder");
        jobConfig.setTriggerGroupName("triggerJobRepairOrder-group");
        jobConfig.setCron(quartzCronConfig.getJobrepairordercron());
        return jobConfig;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        log.info("repairOrder.start");
    }
}
