package com.example.demo.job;

import com.example.demo.config.QuartzCronConfig;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Getter
@Setter
public abstract class JobBase implements Job {
    @Autowired
    public QuartzCronConfig quartzCronConfig;
    public abstract JobConfig getJobConfig();
}
