package com.example.demo.job;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
public class JobConfig {
    private String jobName;//任务名称
    private String jobGroupName;//任务组名称
    private String triggerName;//触发器名称
    private String triggerGroupName;//触发器组名称
    private String cron;//cron表达式
}
