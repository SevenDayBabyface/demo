package com.example.demo.config;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "quartzcron-config")
@Component
@Getter
@Setter
@Slf4j
public class QuartzCronConfig {
    public String jobdemocron;
    public String jobrepairordercron;//补单定时任务执行的间隔时间
    public Integer jobrepairorder_hoursago;//补单定时任务处理多少小时之前的单子
}
