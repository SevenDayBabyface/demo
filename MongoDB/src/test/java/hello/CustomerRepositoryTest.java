package hello;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerRepositoryTest {
    @Autowired
    private CustomerRepository repository;

    @Test
    public void test() throws Exception {
        System.out.println(repository);
        System.out.println(repository.getClass());

        repository.deleteAll();

        repository.save(new Customer("Alice", "Smith", 9));
        repository.save(new Customer("Bob", "Smith", 8));

//        Customer customer = repository.findByFirstName("Alice");
//        System.out.println(customer);
//        Customer customer2 = repository.findByAgeLessThan(8);
//        System.out.println(customer2);
//
//        Customer customer3 = repository.findByAgeLessThan(9);
//        System.out.println(customer3);
//
//        List<Customer> customers = repository.findByAgeLessThanEqual(9);
//        System.out.println(customers);

        List<Customer> customers = repository.findByLastName("Smith");
        System.out.println(customers);

        List<Customer> customers2 = repository.findByLastNameAndAgeLessThan("Smith", 9);
        System.out.println(customers2);

//        List<Customer> customers3 = repositoryCustom.aaLastName_AgeLt("Smith", 9);
//        System.out.println(customers3);
        List<Customer> customers4 = repository.getLastName_AgeLt("Smith", 9);
        System.out.println(customers4);
    }
}
