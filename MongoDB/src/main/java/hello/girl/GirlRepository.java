package hello.girl;

import hello.Person;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

//@RepositoryRestResource(collectionResourceRel = "girl", path = "girl")
//http://localhost:8080/girls
public interface GirlRepository extends MongoRepository<Girl, String> {
    List<Person> findByLastName(@Param("name") String name);
}
