package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

public class CustomerRepositoryImpl implements CustomerRepositoryCustom {
    @Autowired
    MongoTemplate template;

    @Override
    public List<Customer> getLastName_AgeLt(String lastName, int age) {
        Query query = new Query(Criteria.where("lastName").is(lastName).and("age").lt(age));
        return template.find(query, Customer.class);
    }
}
