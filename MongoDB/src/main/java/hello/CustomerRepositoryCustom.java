package hello;

import java.util.List;

public interface CustomerRepositoryCustom {
    List<Customer> getLastName_AgeLt(String lastName, int age);
}
