package hello;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CustomerRepository extends MongoRepository<Customer, String>, CustomerRepositoryCustom {

    Customer findByFirstName(String firstName);

    Customer findByAgeLessThan(int age);

    List<Customer> findByAgeLessThanEqual(int age);

    List<Customer> findByLastName(String lastName);

    /**
     * findBy+LastName+And+(Age+LessThan)
     * findBy+property(+Logical keyword expressions)+And+property(+Logical keyword expressions)+And+property(+Logical keyword expressions)
     */
    List<Customer> findByLastNameAndAgeLessThan(String lastName, int age);
}
