package hello;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class BookAspect {

    //在方法上面使用注解完成增强配置
    @Before(value = "execution(* hello.Book.*(..))")
    public void before1(){
        System.out.println("前置增强...");
    }


}
