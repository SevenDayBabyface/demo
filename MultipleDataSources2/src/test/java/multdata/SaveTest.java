package multdata;

import lombok.extern.slf4j.Slf4j;
import multdata.service.SaveTestService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationTest.class)
@Slf4j
public class SaveTest {
    @Autowired
    SaveTestService saveTestService;

    @Test
    public void saveStimulate() {
        saveTestService.saveStimulate();
    }

    @Test
    public void saveZoo() {
        saveTestService.saveZoo();
    }

}
