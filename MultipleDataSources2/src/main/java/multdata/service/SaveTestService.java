package multdata.service;

import multdata.entity.stimulate.Bullet;
import multdata.entity.stimulate.Gun;
import multdata.entity.zoo.Panda;
import multdata.entity.zoo.Person;
import multdata.repository.stimulate.BulletRepository;
import multdata.repository.stimulate.GunRepository;
import multdata.repository.zoo.PandaRepository;
import multdata.repository.zoo.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SaveTestService {
    @Autowired
    BulletRepository bulletRepository;
    @Autowired
    GunRepository gunRepository;
    @Autowired
    PandaRepository pandaRepository;
    @Autowired
    PersonRepository personRepository;

    private void saveBullet() {
        Bullet entity = new Bullet();
        entity.setName("宝宝");
        bulletRepository.save(entity);
    }

    private void saveGun() {
        Gun entity = new Gun();
        entity.setName("亲爱的");
        gunRepository.save(entity);
    }

    private void savePanda() {
        Panda entity = new Panda();
        entity.setName("小姐姐");
        pandaRepository.save(entity);
    }

    private void savePerson() {
        Person entity = new Person();
        entity.setName("小姐姐呀");
        personRepository.save(entity);
    }

    @Transactional
    public void saveStimulate() {
        saveBullet();
//        int i = 1 / 0;
        saveGun();
    }

    @Transactional("transactionManagerZoo")//指定“限定符”
    public void saveZoo() {
        savePanda();
        savePerson();
    }
}
