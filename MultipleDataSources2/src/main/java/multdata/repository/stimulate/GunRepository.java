package multdata.repository.stimulate;

import multdata.entity.stimulate.Gun;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GunRepository extends JpaRepository<Gun, Long> {

}
