package multdata.repository.stimulate;

import multdata.entity.stimulate.Bullet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BulletRepository extends JpaRepository<Bullet, Long> {

}
