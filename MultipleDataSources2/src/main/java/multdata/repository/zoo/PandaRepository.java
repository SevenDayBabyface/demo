package multdata.repository.zoo;

import multdata.entity.zoo.Panda;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PandaRepository extends JpaRepository<Panda, Long> {

}
